philologue
==========
Quelques styles et commandes utiles pour ma thèse

- philologue.sty -> styles et commandes généralistes pour ma thèse
- handout-philologue.sty -> un complément à handout, pour les exposer
- biblatex-philologue -> des "styles" biblatex